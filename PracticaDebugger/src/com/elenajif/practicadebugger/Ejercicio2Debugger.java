package com.elenajif.practicadebugger;

import java.util.Scanner;

public class Ejercicio2Debugger {

	public static void main(String[] args) {
		/*
		 * Establecer un breakpoint en la primera instruccion y avanzar instruccion a
		 * instruccion (step into) analizando el contenido de las variables
		 */

		Scanner lector;
		int numeroLeido;
		int resultadoDivision;

		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();
		/*
		 * El error lo da la variable i que llega un punto que vale 0 por lo que al
		 * dividir un numero entre ese 0 da un error debido a que no existe una solucion
		 * como tal
		 */

		/*
		 * La solucion seria cambiar la sentencia en la que se detiene el bucle
		 * cambiando el >= por un > asi no llegara a 0, se quedara en 1
		 */

		for (int i = numeroLeido; i >= 0; i--) {
			resultadoDivision = numeroLeido / i;
			System.out.println("el resultado de la division es: " + resultadoDivision);
		}

		lector.close();
	}

}

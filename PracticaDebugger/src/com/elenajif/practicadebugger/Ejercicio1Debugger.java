package com.elenajif.practicadebugger;

import java.util.Scanner;

public class Ejercicio1Debugger {

	public static void main(String[] args) {
		/*
		 * Establecer un breakpoint en la primera instruccion y avanzar instruccion a
		 * instruccion (step into) analizando el contenido de las variables
		 * 
		 */

		Scanner input;
		int cantidadVocales, cantidadCifras;
		String cadenaLeida;
		char caracter;

		cantidadVocales = 0;
		cantidadCifras = 0;

		System.out.println("Introduce una cadena de texto");
		input = new Scanner(System.in);
		cadenaLeida = input.nextLine();
		/*
		 * el error se produce porque al introducir una cadena de texto y al recorrerla
		 * con un bucle llega al punto en el que la variable i(del bucle for) llega a
		 * una posicion fuera de la cadena debido a que al empezar la posicion en 0 la
		 * ultima posicion es la longitud - 1 por lo que al valer i la longitud en si al
		 * pasar por el if para saber si es vocal o cifra salta una excepcion por estar
		 * i fuera de las posiciones de la cadena
		 */

		/*
		 * La solcuion seria que en la sentencia que indica cuando finaliza el bucle
		 * cambiar el menor igual por un menor unicamente ya que asi llegar� hastal la
		 * longitud - 1 que es la ultima posicion y se detendra el bucle ahi
		 */
		for (int i = 0; i <= cadenaLeida.length(); i++) {
			caracter = cadenaLeida.charAt(i);
			if (caracter == 'a' || caracter == 'e' || caracter == 'i' || caracter == 'o' || caracter == 'u') {
				cantidadVocales++;
			}
			if (caracter >= '0' && caracter <= '9') {
				cantidadCifras++;
			}
		}
		System.out.println(cantidadVocales + " y " + cantidadCifras);

		input.close();
	}
}
